procedure learnskills;
	var i: integer;
	begin with Engine, User do begin 
		for i:=100 to 2000 do 
			while engine.LearnSkill (i) do engine.LearnSkill (i); 
		end; 
	end;
	
function fightInMuseum: Boolean;
begin
	Result:=false;
	Print('Going to kill Intruders');
	Engine.LoadZone('museum');
	Engine.LoadConfig('HumanWizard3');
	while Engine.QuestStatus(10542,5) <> true do
	begin
		Engine.FaceControl(0,true);
	end;
	Print('Finished killing Intruders');
	Engine.FaceControl(0,false);
end;

function equipSameID(itemId: integer): Boolean;
var i : Integer;
begin
	for i := 0 to Inventory.User.Count-1 do begin
		if (Inventory.User.Items(i).id = itemId) then 
		begin
			Engine.UseItem(Inventory.User.Items(i));
		end;
	end;
end;

function oneToTwenty: Boolean;
begin
  Result:=false;
  Print('Going to speak to Pantheon');
  Print('Starting first quest');
  Engine.SetTarget(32972);
  Engine.DlgOpen;
  Engine.DlgSel('Quest');
  delay(500);
  Engine.DlgSel(1);
  delay(500);
  Engine.DlgSel(1);
  
  Print('Move to Town Square Center');
  Engine.MoveTo(-114662, 259436, -1225);
  Engine.MoveTo(-114289, 256589, -1310);
  Delay(30000); // Animation  
  Print('Going to speak to Theodore');
  Engine.MoveTo(-114362, 255136, -1551);
  Engine.SetTarget(32975);
  Engine.DlgOpen;
  delay(500);
  Engine.DlgSel('Quest');
  delay(500);
  Engine.DlgSel(1);
  delay(500);
  Engine.DlgSel(1);
  delay(1000);
  Print('First quest completed!');
    
  Print('Starting second quest');
  Engine.DlgOpen;
  delay(500);
  Engine.DlgSel('Quest');
  delay(500);
  Engine.DlgSel(1);
  delay(500);
  Engine.DlgSel(1);
  delay(500);
  Engine.DlgSel(1);

  Print('Going to speak to Shannon');
  Engine.MoveTo(-113160, 254806, -1537);
  Engine.MoveTo(-112191, 255227, -1492);
  Engine.MoveTo(-111463, 255822, -1470);
  Engine.SetTarget(32974);
  Engine.DlgOpen;
  delay(500);
  Engine.DlgSel('Quest');
  delay(500);
  Engine.DlgSel(1);
  delay(500);
  Engine.DlgSel(1);
  delay(500);
  Print('Second quest completed!');

  Print('Starting third quest');
  Engine.DlgOpen;
  delay(500);
  Engine.DlgSel('Quest');
  delay(500);
  Engine.DlgSel(1);
  delay(500);
  Engine.DlgSel(1);
  delay(500);
  Engine.DlgSel(1);
  delay(500);
  Engine.DlgSel(1);

  Print('Going to kill 4 Training Dummies');
  Engine.LoadZone('TrainingDummies');
  Engine.LoadConfig('HumanWizard3');
  Engine.MoveTo(-111396, 255674, -1470);
  while Engine.QuestStatus(10541,2) <> true do
  begin
    Engine.FaceControl(0,true);
  end;
  Print('Finished killing 4 dummies');
  Engine.FaceControl(0,false);
  Print('Going to speak to Shannon');
  Engine.SetTarget(32974);
  Engine.DlgOpen;
  delay(500);
  Engine.DlgSel('Quest');
  delay(500);
  Engine.DlgSel(1);
  delay(500);
    
  Print('Getting first buffs!');
  Engine.SetTarget(32981);
  Engine.DlgOpen;
  Delay(500);
  Engine.DlgSel(1);
  Delay(500);
  Engine.DlgSel(1);
  Delay(500);
  Engine.DlgSel(1);
  Delay(500);
  Engine.DlgSel(1);

  Print('We are buffed, ready to fight training dummies again');
  if Engine.QuestStatus(10541,4) then begin
    Engine.LoadZone('TrainingDummies');
    Engine.LoadConfig('HumanWizard3');
    Engine.MoveTo(-111322, 255580, -1470);
      
    while Engine.QuestStatus(10541,5) <> true do
    begin
      Engine.FaceControl(0,true);
    end;
    Print('Finished killing 4 dummies');
    Engine.FaceControl(0,false);
    Print('Going back to Shanon finish quest');
    Engine.SetTarget(32974);
    Engine.DlgOpen;
    Delay(500);
    Engine.DlgSel('Quest');
    Delay(500);
    Engine.DlgSel(1);
    Print('Quest completed! You are now Level 5');
  end;
  
	Print('Starting quest 5');
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(2000);
	Print('Speak to Toyron');
	Engine.SetTarget(33004);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
  
	//Move to 1st book
	Engine.MoveTo(-113921, 244640, -8000);
	Engine.SetTarget('Desk');
	Engine.DlgOpen;
	if Engine.QuestStatus(10542,4) then
	begin 
		fightInMuseum 
	end
	else
		//Move to 2nd book
		Engine.MoveTo(-114715, 245650, -7994);
		Engine.SetTarget('Desk');
		Engine.DlgOpen;
		if Engine.QuestStatus(10542,4) then
		begin 
			fightInMuseum 
		end
		else
			//Move to 3rd book
			Engine.MoveTo(-115568, 244697, -7992);
			Engine.SetTarget('Desk');
			Engine.DlgOpen;
			if Engine.QuestStatus(10542,4) then
			begin 
				fightInMuseum  
			end
			else
				//Move to 4th book
				Engine.MoveTo(-114711, 243707, -7990);
				Engine.SetTarget('Desk');
				Engine.DlgOpen;
				begin 
					fightInMuseum
				end;
				
	Print('Intruders defeated - Waiting for expiry of the dungeon');
	Delay(90000);
	Print('Teleported to Shannon - finishing Quest.');
	Engine.SetTarget(32974);
	Engine.DlgOpen;
    Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Print('Quest 6 completed! Level 6 reached');
	
	// Quest 7
	Print('Starting: Quest 7');
	Engine.SetTarget(33004);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Print('Going to speak to Wilford');
	Engine.MoveTo(-112924, 254707, -1552);
	Engine.MoveTo(-113675, 254868, -1537);
	Engine.MoveTo(-113623, 255288, -1524);
	Engine.SetTarget(30005);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Print('Completed: Quest 7');
	
	Engine.UseItem(7816); // Put on Apprentice Adventurer's Staff
	
	// Quest 8
	Print('Starting: Quest 8');
	Engine.SetTarget(30005);
	Engine.DlgOpen;
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Print('Speak with Katerina');
	Engine.MoveTo(-113411, 255969, -1496);
	Engine.SetTarget(30004);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Print('Speak with Lector');
	Engine.MoveTo(-113781, 255072, -1532);
	Engine.MoveTo(-113645, 253635, -1524);
	Engine.SetTarget(30001);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Print('Speak with Jackson');
	Engine.MoveTo(-113751, 253371, -1524);
	Engine.MoveTo(-113606, 253159, -1524);
	Engine.SetTarget(30002);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Print('Speak with Trevor');
	Engine.MoveTo(-115042, 253564, -1523);
	// Speak
	Engine.SetTarget(32166);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	//Print('Speak with Rivian'); // ELF MYSTIC
	Print('Speak with Franco'); // HUMAN MYSTIC
	
	// Travel
	Engine.MoveTo(-114918, 254725, -1557);
	Engine.MoveTo(-115925, 254886, -1536);
	Engine.MoveTo(-117842, 255905, -1353);
	//Engine.MoveTo(-117670, 256322, -1353); // ELF MYSTIC
	Engine.MoveTo(-117943, 255930, -1353); // HUMAN MYSTIC
	// Speak
	Engine.SetTarget(32147); // ELF MYSTIC
	Engine.SetTarget(32153); // HUMAN MYSTIC
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Print('Completed: Quest 8');
	
	// Equip new items
	equipSameID(112);
	Engine.UseItem('Necklace Of Knowledge');
	Engine.UseItem('Leather Tunic');
	Engine.UseItem('Leather Stockings');
	
	// Quest 9
	Print('Starting: Quest 9');
	// Speak
	//Engine.SetTarget(32147); // ELF MYSTIC
	Engine.SetTarget(32153); // HUMAN MYSTIC
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Print('Speak with Milia');
	// Travel
	Engine.MoveTo(-117826, 255892, -1353);
	Engine.MoveTo(-117029, 255505, -1325);
	Engine.MoveTo(-115569, 254788, -1545);
	Engine.MoveTo(-114897, 254824, -1556);
	Engine.MoveTo(-113545, 256540, -1532);
	// Speak
	Engine.SetTarget(30006);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Print('Speak with Lakcis');
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(10000); // WAIT FOR TELEPORT
	// Speak
	Engine.SetTarget(32977);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Print('Completed: Quest 9');
	
	// Equip new items
	equipSameID(875);
	
	// Quest 10
	Print('Starting: Quest 10 -  Certification of the Seeker');
	// Speak
	Engine.SetTarget(32977);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	
	Print('Speak with Chesha');
	Engine.SetTarget(33180);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel(1);
	Delay(12000);
	Engine.MoveTo(-115008, 230190, -1681);
	Engine.SetTarget(33197);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel(1);
	Delay(1000);
	Engine.MoveTo(-115234, 237383, -3115);
	Engine.SetTarget(33449);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	// Fight
	Print('Going to kill 10 Stalkers and 10 Crawlers');
	Engine.LoadZone('ruinsofyesagira1');
	Engine.LoadConfig('HumanWizard3');
	while Engine.QuestStatus(10362,3) <> true do
    begin
      Engine.FaceControl(0,true);
    end;
    Print('Finished killing 10 Stalkers and 10 Crawlers');
    Engine.FaceControl(0,false);
	Print('Step: Speak with Nagel');
	// Travel
	Engine.MoveTo(-115675, 236639, -3115);
	Engine.MoveTo(-114723, 235373, -3115);
	Engine.MoveTo(-113727, 236334, -3069);
	Engine.MoveTo(-113517, 237212, -3069);
	// Speak
	Engine.SetTarget(33450);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Print('Completed: Quest 10 - Certification of the Seeker');
	
	// Equip item
	Engine.UseItem('Gloves');
	
	// Learn Skills
	begin learnskills end;
	
	
	// Quest 11
	Print('Starting: Quest 11 -  Request of the Seeker');
	// Speak
	Engine.SetTarget(33450);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	// Teleport
	Engine.SetTarget(33182);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel(1);
	Delay(5000);
	// Travel
	Engine.MoveTo(-118744, 232690, -2930);
	Engine.MoveTo(-117633, 232668, -2930);
	// Fight
	Print('Step: Collect 15 Husk Distribution Reports');
	Engine.LoadZone('ruinsofyesagira2');
	Engine.LoadConfig('HumanWizard3');
	while Engine.QuestStatus(10363,2) <> true do
    begin
      Engine.FaceControl(0,true);
    end;
    Engine.FaceControl(0,false);
	Print('Step Complete: Collect 15 Husk Distribution Reports');
	Print('Step: Speak with Celin');
	// Travel
	Engine.MoveTo(-116578, 233996, -2930);
	// Speak
	Engine.SetTarget(33451);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Print('Completed: Quest 11 -  Request of the Seeker');
	
	// Equip item
	Engine.UseItem('Wooden Helmet');
	
	
	// Quest 12
	Print('Starting: Quest 12 -  Obligation of the Seeker');
	// Speak
	Print('Step: Speak with Celin');
	Engine.SetTarget(33451);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Print('Step: Speak with Walter');
	// Teleport
	Engine.SetTarget(33189);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel(1);
	Delay(5000);
	// Travel
	Engine.MoveTo(-116483, 239207, -2815);
	Print('Step: Speak with Walter');
	Engine.SetTarget(33452);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Print('Step: Collect 5 Dirty Piece of Paper');
	// Travel
	Engine.MoveTo(-116164, 240564, -2769);
	Engine.MoveTo(-117472, 240616, -2769);
	// Fight
	Engine.LoadZone('ruinsofyesagira3');
	Engine.LoadConfig('HumanWizard3');
	while Engine.QuestStatus(10364,3) <> true do
    begin
      Engine.FaceControl(0,true);
    end;
    Engine.FaceControl(0,false);
	Print('Step Complete: Collect 5 Dirty Piece of Paper');
	Print('Step: Travel to Dep');
	// Travel
	Engine.MoveTo(-118235, 239315, -2769);
	// Teleport
	Engine.SetTarget(33191);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel(1);
	Delay(5000);
	Print('Step: Speak with Dep');
	Engine.SetTarget('Dep');
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Print('Completed: Quest 12 -  Obligation of the Seeker');
	
	// Equip items
	Engine.UseItem('Leather Shoes');
	
	// Learn Skills
	
	begin learnskills end;
	
	
	// Quest 13
	Print('Starting: Quest 13 -  For the Searchdog King');
	// Speak
	Print('Step: Speak with Dep');
	Engine.SetTarget('Dep');
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	
	Print('Step: Go to collect 20 King''s Tonic');
	// Travel
	Engine.MoveTo(-111051, 238143, -2951);
	Engine.MoveTo(-111167, 239734, -2952);
	// Fight
	Print('Step: Collect 20 King''s Tonic');
	Engine.LoadZone('ruinsofyesagira4');
	Engine.LoadConfig('HumanWizard3');
	while Engine.QuestStatus(10365,2) <> true do
    begin
      Engine.FaceControl(0,true);
    end;
    Engine.FaceControl(0,false);
	Print('Step Complete: Collect 20 King''s Tonic');
	Print('Step: Go to Speak with Sebion');
	// Travel
	Engine.MoveTo(-112243, 240302, -2952);
	// Teleport
	Engine.SetTarget(33188);
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel(1);
	Delay(5000);
	// Travel
	Engine.MoveTo(-112468, 234309, -3129);
	Engine.MoveTo(-111841, 231832, -3191);
	Print('Step: Speak with Dep');
	Engine.SetTarget('Sebion');
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
	Delay(500);
	Engine.DlgSel(1);
	Delay(500);
	Engine.DlgSel(1);
	Print('Completed: Quest 13 -  For the Searchdog King');
	
	// Quest 13
	Print('Starting: Quest 13 -  For the Searchdog King');
	// Speak
	Print('Step: Speak with Dep');
	Engine.SetTarget('Dep');
	Engine.DlgOpen;
	Delay(500);
	Engine.DlgSel('Quest');
end;

begin oneToTwenty
end.
